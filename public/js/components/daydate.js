function weekDays() {

    var now = new Date();

    var elements = document.querySelectorAll(".day");

    elements.forEach(function(day) {

        var dd = String(now.getDate()).padStart(2, '0');
        var mm = String(now.getMonth() + 1).padStart(2, '0');
        var yyyy = now.getFullYear();

        now.setDate(now.getDate() + 1);
        dateToDisplay = dd + '/' + mm + '/' + yyyy;
        day.querySelector('.day-date').innerHTML = dateToDisplay;
    });
}
weekDays();

function dateName() {

    var a = new Date();

    var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    var elements = document.querySelectorAll(".day");

    elements.forEach(function(day) {

        var currentWeekdayName = weekday[a.getDay()];
        a.setDate(a.getDate() + 1);
        displayName = currentWeekdayName;
        day.querySelector(".day-name").innerHTML = displayName;
    });
}
dateName();

function count() {

    var hours = new Array(24);

    var toAdd = document.querySelector(".timetable");

    for (var i = 0; i < hours.length; i++) {
        var newDiv = document.createElement('div');
        newDiv.classList.add('hour');

        if (i < 10) {
            newDiv.innerHTML += "0" + i + ":00" + "<br >";
        } else {
            newDiv.innerHTML += i + ":00" + "<br >";
        }

        toAdd.appendChild(newDiv);

        /*var addToDays = document.querySelectorAll(".day");

        addToDays.forEach(function(day)) {
            var newDiv2 = document.createElement('div');
            newDiv2.classList.add('note-day');

            addToDays.appendChild(newDiv2);
        }*/
    }

}
count();