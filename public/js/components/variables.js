class Calendar {

    constructor() {
        this.defaults = {
            dayClass: ".day-date",
            weekdays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysClass: ".day",
            dayNameClass: ".day-name",
            dayHour: ".hour",
            timetableClass: ".timetable"
        }

        this.displayWeekdaysName();
        this.dateName();
        this.count();
    }

    displayWeekdaysName = () => {

        var now = new Date();

        var elements = document.querySelectorAll.this.daysClass;

        elements.forEach(function(day) {

            var dd = String(now.getDate()).padStart(2, '0');
            var mm = String(now.getMonth() + 1).padStart(2, '0');
            var yyyy = now.getFullYear();

            now.setDate(now.getDate() + 1);
            dateToDisplay = dd + '/' + mm + '/' + yyyy;
            day.querySelector.this.dayClass.innerHTML = dateToDisplay;
        });
    }

    dateName = () => {

        var a = new Date();

        var elements = document.querySelectorAll.this.daysClass;

        elements.forEach(function(day) {

            var currentWeekdayName = this.weekdays[a.getDay()];
            a.setDate(a.getDate() + 1);
            displayName = currentWeekdayName;
            day.querySelector.this.dayNameClass.innerHTML = displayName;
        });
    }

    count = () => {

        var hours = new Array(24);

        var toAdd = document.querySelector.this.timetableClass;

        for (var i = 0; i < hours.length; i++) {
            var newDiv = document.createElement('div');
            newDiv.classList.add.this.dayHour;

            if (i < 10) {
                newDiv.innerHTML += "0" + i + ":00" + "<br >";
            } else {
                newDiv.innerHTML += i + ":00" + "<br >";
            }
            toAdd.appendChild(newDiv);
        }

    }
}